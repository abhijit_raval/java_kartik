package collections;

public class WordCount {

	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + charCount;
		result = prime * result + ((character == null) ? 0 : character.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		WordCount other = (WordCount) obj;
		if (character == null) {
			if (other.character != null)
				return false;
		} 
		if (character.equals(other.character))
			return true;
		return false;
	}

	private int charCount;
	private String character;

	public int getCharCount() {
		return charCount;
	}

	public void setCharCount(int charCount) {
		this.charCount = charCount;
	}


	public WordCount(int charCount, String character) {
		super();
		this.charCount = charCount;
		this.character = character;
	}

	public String getCharacter() {
		return character;
	}

	public void setCharacter(String character) {
		this.character = character;
	}

	

}
