package collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class MainClass {
	public static void main(String[] args) {
		String s = "sjlkfadjfl dlakjfhdlakjf kjadbhfkjda ewrewttwbncvnbqweoipoi mnasx";
		List<WordCount> list = new ArrayList<WordCount>();
		char[] character = s.toCharArray();
		for (int i = 0; i < character.length; i++) {
			char c = character[i];
			WordCount wc = new WordCount(0, String.valueOf(c));
			if (list.contains(wc)) {
				int index = list.indexOf(wc);
				WordCount original = list.get(index);
				original.setCharCount(original.getCharCount() + 1);
			} else {
				list.add(new WordCount(1, String.valueOf(c)));
			}
		}
		//ascending
		Collections.sort(list, new Comparator<WordCount>() {
			@Override
			public int compare(WordCount o1, WordCount o2) {
				if (o1.getCharCount() == o2.getCharCount())
					return o1.getCharacter().compareTo(o2.getCharacter());
				return o1.getCharCount() > o2.getCharCount() ? 1 : -1;
			}
		});
		//descending
//		  Collections.sort(list, new Comparator<WordCount>() {
//		  
//		  @Override public int compare(WordCount o1, WordCount o2) {
//		  if(o1.getCharCount()==o2.getCharCount()) 
//			  return o2.getCharacter().compareTo(o1.getCharacter()); 
//		  return o2.getCharCount()>o1.getCharCount() ?1:-1; } });
//		 
		Iterator<WordCount> it = list.iterator();
		while (it.hasNext()) {
			WordCount w1 = it.next();
			System.out.println(w1.getCharacter() + " " + w1.getCharCount());
		}
	}
}
